import { Body, Controller, Post, Res } from "@nestjs/common";
import { Response } from 'express';

import { CreateUserException, CreateUserUsecase } from "../../domain/usecases/create-user/create-user.usecase";
import { UserServiceImpl } from "./user.service";
import { HttpPresenter } from "src/domain/base/http-presenter";
import { CreateUserRequest } from "src/domain/usecases/create-user/create-user.request";
import { CreateUserResponse } from "src/domain/usecases/create-user/create-user.response";


@Controller('/user')
export class UserController {
  constructor(private userService: UserServiceImpl) {}

  @Post('/register')
  async login(@Body() body: any,@Res() res: Response): Promise<any> {
    const presenter: HttpPresenter<CreateUserException,CreateUserResponse> = new HttpPresenter<CreateUserException,CreateUserResponse>(res);
    await (new CreateUserUsecase(presenter)).execute(this.userService,body as unknown as CreateUserRequest);
    return presenter.render();
  }
}