import { User } from "../entities/user";

export interface IUserService extends IService {
    createUser(username:string): Promise<User>;
}