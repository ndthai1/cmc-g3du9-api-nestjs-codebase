import { IInputBoundary } from "src/domain/base/interfaces/input-boundary";
import { IOutputBoundary } from "src/domain/base/interfaces/output-boundary";
import { HttpException } from "@nestjs/common";
import { IUserService } from "../user.service";
import { CreateUserRequest } from "./create-user.request";
import { IResponse } from "@core/interfaces/response";
export type CreateUserException = HttpException;
export class CreateUserUsecase implements IInputBoundary<IUserService,CreateUserRequest> {
    constructor(private presenter: IOutputBoundary<CreateUserException,IResponse>) {

    }
    async execute(service: IUserService, request:CreateUserRequest): Promise<void> {
        try {
            const user = await service.createUser(request.userName);
            if(user) 
                this.presenter.accept(user as unknown as IResponse);
        }
        catch (e){
            this.presenter.reject(new HttpException('Unknown error',400));
        }
    }
}