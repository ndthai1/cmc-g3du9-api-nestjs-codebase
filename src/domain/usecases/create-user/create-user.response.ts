import { IResponse } from "@core/interfaces/response";

export class CreateUserResponse implements IResponse {
    status: string;
    code: string;
    data?: any;
    error?:any;
}