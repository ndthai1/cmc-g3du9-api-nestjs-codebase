import { HttpException } from "@nestjs/common";

export interface IOutputBoundary<E extends HttpException, S extends IResponse> {
    accept(response: S): void;
    reject(error: E): void;
}