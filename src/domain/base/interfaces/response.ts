export interface IResponse {
    status: string;
    code: string;
    data?: any;
    error?:any;
}