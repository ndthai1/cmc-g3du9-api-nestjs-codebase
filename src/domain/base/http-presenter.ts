// Nestjs modules
import { HttpException, HttpStatus, Res } from "@nestjs/common";

//Packages
import { Response } from 'express';

import { IOutputBoundary } from "./interfaces/output-boundary";
import { IResponse } from "./interfaces/response";


export class HttpPresenter<E extends HttpException, S extends IResponse> implements IOutputBoundary<E,S>{
    private data: S;
    private error: E;
    constructor(private response: Response) {
        
    }
    accept(data: S): void {
        this.data = data;
    }
    reject(error: E): void {
        this.error = error;
    }
    render(): void {
        if (this.error) this.response.status(this.error.getStatus()).json(this.error);
        else this.response.status(HttpStatus.OK).json(this.data);
    }

}