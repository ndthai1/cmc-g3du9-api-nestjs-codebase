import { BaseEntity } from "src/domain/base/base-entity";

export class User extends BaseEntity {

    constructor(private firstName: string,
        private lastName: string,
        private userName: string) {
        super();
    }
}